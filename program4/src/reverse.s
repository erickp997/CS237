*----------------------------------------------------------------------
* Pseudocode:
*
* void reverse(char &in, char &out, int count){
* if(count == 0){
*     return; 
* 
* reverse(in+1,out,--count);
* *(out+count) = *in;
* }
*----------------------------------------------------------------------

        ORG     $6000

in:     EQU     8
out:    EQU     12
count:  EQU     16

reverse:
        link    A6,#0
        movem.l A0-A1/D1,-(SP)
        movea.l in(A6),A0       * Offset for in
        movea.l out(A6),A1      * Offset for out
        move.w  count(A6),D1    * Offset for count
        tst     D1
        beq     done
        subq.w  #1,D1       * --count
        move.w  D1,-(SP)    * Push count
        pea     (A1)        * Push output
        pea     (A0)        * in+1
        addq.l  #1,(SP)      
        jsr     reverse     * Call
        adda.l  #10,SP      * Pop off the stack
        add.l   D1,A1       * count + out
        move.b  (A0),(A1)   * input into output address
done:   movem.l (SP)+,A0-A1/D1
        unlk    A6
        rts
        end
