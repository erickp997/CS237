*----------------------------------------------------------------------
* Programmer: Erick Pulido
* Class Account: cssc0237
* Assignment or Title: Program 4
* Filename: prog4.s
* Date completed: 5/8/17
*----------------------------------------------------------------------
* Problem statement: Input a string and output the string reversed
* Input: A string
* Output: The string reversed
* Error conditions tested: Fixed incorrect address errors
* Included files: prog4.s reverse.s
* Method and/or pseudocode: Handwritten Java Code to Assembly
* References: CS 237 Supplementary Material, Lecture Notes, TA's
*----------------------------------------------------------------------
*
        ORG     $0
        DC.L    $3000           * Stack pointer value after a reset
        DC.L    start           * Program counter value after a reset
        ORG     $3000           * Start at location 3000 Hex
*
*----------------------------------------------------------------------
*
#minclude /home/cs/faculty/riggins/bsvc/macros/iomacs.s
#minclude /home/cs/faculty/riggins/bsvc/macros/evtmacs.s
*
*----------------------------------------------------------------------
*
* Register use
*
*----------------------------------------------------------------------
*
reverse:    EQU     $6000
start:  initIO          * Initialize (required for I/O)
	    setEVT			* Error handling routines
*	    initF			* For floating point macros only	

        lineout     title
        lineout     prompt
        linein      buffer
        move.l      D0,D1       * Store string length for later
        move.w      D1,-(SP)    * Count
        pea         answer      * Out
        pea         buffer      * In
        jsr         reverse
        adda.l      #10,SP
        lea         answer,A0   * Move the answer to A0
        adda.l      D1,A0       * Point to end of string
        clr.b       (A0)        * Null terminate answer
        lineout     skipln
        lineout     msg
        lineout     answer      * Prints below message instead of after       

        break                   * Terminate execution
*
*----------------------------------------------------------------------
*       Storage declarations

title:          dc.b    'Program #4, Erick Pulido, cssc0237',0
skipln          dc.b    0
prompt:         dc.b    'Enter a string:',0
buffer:         ds.b    80
msg:            dc.b    'Here is the string backwards:',0
answer:         ds.b    80

        end
