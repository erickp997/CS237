*----------------------------------------------------------------------
* Programmer: Erick Pulido
* Class Account: cssc0237
* Assignment or Title: Program 2
* Filename: prog2.s
* Date completed: 3/19/17
*----------------------------------------------------------------------
* Problem statement: Calculate the monthly payment for a loan
* Input: The amount borrowed, the APR, and length in months
* Output: The monthly payment
* Error conditions tested: None
* Included files: prog2.s
* Method and/or pseudocode: None
* References: CS237 Course Reader
*----------------------------------------------------------------------
*
        ORG     $0
        DC.L    $3000           * Stack pointer value after a reset
        DC.L    start           * Program counter value after a reset
        ORG     $3000           * Start at location 3000 Hex
*
*----------------------------------------------------------------------
*
#minclude /home/cs/faculty/riggins/bsvc/macros/iomacs.s
#minclude /home/cs/faculty/riggins/bsvc/macros/evtmacs.s
*
*----------------------------------------------------------------------
*
* Register use
*
*----------------------------------------------------------------------
*
start:  initIO                  * Initialize (required for I/O)
        setEVT                  * Error handling routines
       	initF                   * For floating point macros only

        lineout         title
        lineout         skipln
        lineout         prompt1
        floatin         buffer
        cvtaf           buffer,D1   * Store principle amount as float
        lineout         prompt2
        floatin         buffer
        cvtaf           buffer,D2   * Store rate as a float
        lineout         prompt3
        floatin         buffer
        cvtaf           buffer,D3   * Store months as a float
        moveq           #100,D7
        itof            D7,D7       * 100 as a float
        fdiv            D7,D2       * rate / 100
        moveq           #12,D7
        itof            D7,D7       * 12 in float
        fdiv            D7,D2       * rate / 12 because it is a monthly rate
        move.l          D2,D6
        moveq           #1,D7
        itof            D7,D7       * 1 as a float
        fadd            D7,D2       * 1+r
        fpow            D2,D3       * (1+r)^n and fpow stores in D0
        move.l          D0,D5
        fmul            D5,D6       * Compute r x (1+r)^n
        fsub            D7,D5       * Compute (1+r)^n - 1
        fdiv            D5,D6       * Compute numerator / denominator
        fmul            D1,D6       * Compute p * previous operation
        move.l          D6,D0       * Gets ready to convert monthly pay
        cvtfa           payment,#2  * Output monthly pay and null terminates
        lineout         skipln
        lineout         answer

        break                   * Terminate execution
*
*----------------------------------------------------------------------
*       Storage declarations

title:          dc.b    'Program #2, Erick Pulido, cssc0237',0
skipln:         dc.b    0
prompt1:        dc.b    'Enter the amount of the loan:',0
buffer:         ds.b    85
prompt2:        dc.b    'Enter the annual percentage rate:',0
prompt3:        dc.b    'Enter the length of the loan in months:',0
answer:         dc.b    'Your monthly payment will be $'
payment:        ds.b    80

        end
