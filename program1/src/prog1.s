*----------------------------------------------------------------------
* Programmer: Erick Pulido
* Class Account: cssc0237
* Assignment or Title: Program 1
* Filename: prog1.s
* Date completed: 2/28/17
*----------------------------------------------------------------------
* Problem statement: Convert user input MM/DD/YYYY to Month Day, Year
* Input: MM/DD/YYYY
* Output: Month name, day, year
* Error conditions tested: None
* Included files: prog1.s
* Method and/or pseudocode: None
* References: CS237 Course Reader
*----------------------------------------------------------------------
*
        ORG     $0
        DC.L    $3000           * Stack pointer value after a reset
        DC.L    start           * Program counter value after a reset
        ORG     $3000           * Start at location 3000 Hex
*
*----------------------------------------------------------------------
*
#minclude /home/cs/faculty/riggins/bsvc/macros/iomacs.s
#minclude /home/cs/faculty/riggins/bsvc/macros/evtmacs.s
*
*----------------------------------------------------------------------
*
* Register use
*
*----------------------------------------------------------------------
*
start:  initIO                  * Initialize (required for I/O)
        setEVT                  * Error handling routines
*       initF                   * For floating point macros only

        lineout         title
        lineout         skipln
        lineout         prompt
        linein          buffer
        cvta2           buffer,#2
        subi.l          #1,D0   * Makes an index for the months
        move.l          D0,D1
        mulu            #10,D0  * Offset the lengths in D0
        lea             month,A0
        adda.l          D0,A0   * Reset pointer to read in MM
        move.l          (A0)+,date
        move.l          (A0)+,date+4
        move.w          (A0)+,date+8
        lea             date,A0
        lea             length,A1
        mulu            #4,D1   * The index multiplied by length
        adda.l          D1,A1   * The storage for the length
        adda.l          (A1),A0 * Moves pointer to read out DD
        move.b          #' ',(A0)+
        move.b          buffer+3,(A0)+
        move.b          buffer+4,(A0)+
        suba.l          #2,A0
        stripp          (A0),#2 * Removes leading 0 in day
        adda.l          D0,A0   * Moves pointer to output , YYYY.
        move.b          #',',(A0)+
        move.b          #' ',(A0)+
        move.b          buffer+6,(A0)+
        move.b          buffer+7,(A0)+
        move.b          buffer+8,(A0)+
        move.b          buffer+9,(A0)+
        move.b          #'.',(A0)+
        clr.b           (A0)    * Null terminate answer string
        lineout         skipln
        lineout         answer

        break                   * Terminate execution
*
*----------------------------------------------------------------------
*       Storage declarations

length:         dc.l    7,8,5,5,3,4,4,6,9,7,8,8
month:          dc.b    'January   '
                dc.b    'February  '
                dc.b    'March     '
                dc.b    'April     '
                dc.b    'May       '
                dc.b    'June      '
                dc.b    'July      '
                dc.b    'August    '
                dc.b    'September '
                dc.b    'October   '
                dc.b    'November  '
                dc.b    'December  '
title:          dc.b    'Program #1, Erick Pulido, cssc0237',0
skipln:         dc.b    0
prompt:         dc.b    'Enter a date in the form MM/DD/YYYY',0
buffer:         ds.b    82
answer:         dc.b    'The date entered is '
date:           ds.b    24

        end
