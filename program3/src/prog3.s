*----------------------------------------------------------------------
* Programmer: Erick Pulido
* Class Account: cssc0237
* Assignment or Title: Program 3
* Filename: prog3.s
* Date completed: 5/1/17 
*----------------------------------------------------------------------
* Problem statement: Convert a number in one base to a different base
* Input: A starting base, a number in that base, and a base to convert
* Output: The number in the new base
* Error conditions tested: Invalid start, finish bases and invalid numbers
* Included files: prog3.s
* Method and/or pseudocode: Handwritten Java code converted to Assembly
* References: CS 237 Supplementary Material, Lecture, Office Hours
*----------------------------------------------------------------------
*
        ORG     $0
        DC.L    $3000           * Stack pointer value after a reset
        DC.L    start           * Program counter value after a reset
        ORG     $3000           * Start at location 3000 Hex
*
*----------------------------------------------------------------------
*
#minclude /home/cs/faculty/riggins/bsvc/macros/iomacs.s
#minclude /home/cs/faculty/riggins/bsvc/macros/evtmacs.s
-   *
*----------------------------------------------------------------------
*
* Register use
*
*----------------------------------------------------------------------
*
start:  initIO                  * Initialize (required for I/O)
	    setEVT                  * Error handling routines
*	    initF                   * For floating point macros only	

        lineout         title
        lineout         skipln
        bra             base1        
bad1:   lineout         basemsg  * Error message for invalid base
base1:  lineout         prompt1  * Collect the input base and validate
        linein          buffer
        stripp          buffer,D0 * Remove any leading zeros from input
        lea             buffer,A0 * For checking byte by byte
        cmpi.b          #1,D0   * Is length less than 1 digit
        blt             bad1
        cmpi.b          #2,D0   * Is length more than 2 digits
        bgt             bad1        
b1tst2: cmpi.b          #$30,(A0)   * Is digit less than 0
        blo             bad1
        cmpi.b          #$39,(A0)+  * moves into next digit for verification
        bhi             bad1
        addi.b          #1,D1   * Counter for the digits in base
        cmp.b           D0,D1
        blt             b1tst2  * If there is another digit, repeat        
        cvta2           buffer,D0   * Convert the number to 2s compliment
        clr.l           D1
        move.w          D0,D1
        cmpi.b          #2,D1   * Checks if base is less than 2
        blo             bad1
        cmpi.b          #16,D1  * Checks if base is greater than 16
        bhi             bad1
        bra             num     * D1 contains base in 2s comp
badnum: lineout         nummsg  * Error message for invalid number
num:    lineout         prompt2
        linein          buffer
        stripp          buffer,D0
        lea             buffer,A1   * A1 now contains the number
        move.w          D0,D2   * move length of number for later use
        subi.w          #1,D2
        move.w          D2,D6   * D2,D6 contains length of the number
chknum: tst.w           D0
        blo             badnum
        cmpi.b          #$30,(A1)   * If digit is less than 0
        blo             badnum
        cmpi.b          #$39,(A1)   * If digit is greater than 9
        bhi             letter
        subi.b          #$30,(A1)   * Convert ascii numbers to decimal numbers
        bra             loop                 
letter: ori.b           #$20,(A1)   * Makes upper case letters lower case
        cmpi.b          #$41,(A1)   * Is the digit less than ascii a
        blo             badnum
        cmpi.b          #$46,(A1)   * Is the digit greater than ascii f
        bhi             badnum
        subi.b          #$57,(A1)   * Convert ascii letters to number values        
loop:   cmp.b           (A1)+,D1    * Is digit valid for base
        bls             badnum  * base <= digit is bad
        dbra            D2,chknum   * Cycle through the input number
	      moveq           #1,D4       * tmp base
        move.b          (A1),D6         
base10: mulu            D4,D6   * base^tmp
        mulu            D1,D4   * base * base for positional notation conversion
        add.l           D4,D5   * Stores number in base 10 to D5
        dbra            D2,base10 
        clr.l           D3
        clr.l           D4
        move.w          D5,D4
        cmpi.w          #$FFFF,D5   * Checks if number is out of the range
        bhi             badnum
        bra             base2
bad2:   lineout         basemsg     * Error message for invalid base
base2:  lineout         prompt3     * Collect the information for base to convert to
        linein          buffer
        stripp          buffer,D0   * Remove any leading zeros from the input
        lea             buffer,A2   * A2 contains the base to convert to
        cmpi.b          #1,D0       * Is length less than 1 digit
        blo             bad2
        cmpi.b          #2,D0   * Is length greater than 2 digits
        bhi             bad2        
b2tst2: cmpi.b          #$30,(A2)   * Validates if the input is 0-9
        blo             bad1
        cmpi.b          #$39,(A2)+
        addi.b          #1,D3   * Counter for digits
        cmp.b           D0,D3
        blt             b2tst2        
cvtb2:  cvta2           buffer,D0   * Convert the number to 2s compliment
        clr.l           D3
        move.w          D0,D3   * D3 contains the 2s complement
        cmpi.b          #2,D3   * Checks if base is less than 2
        blo             bad2
        cmpi.b          #16,D3  * Checks if base is greater than 16
        bhi             bad2                                                       
        lea             newnum,A3       
conv:   ext.l           D5
        andi.l          #$0000FFFF,D5   * Keep byte portion of number
        divu            D3,D5   * Number divided by base 
        swap            D5
        move.b          D5,(A3)+    * Collect remainder and move to A3
        swap            D5
        tst             D5  * Keep dividing until quotient is zero
        bne             conv    * Value is in A3, backwards               
        lea             output,A4
load:   move.b          -(A3),(A4)+ * Begin populating the answer
        dbra            D4,load * Cycles through the length of the number
        clr.b           (A4)    * For null terminating the answer     
more:   lineout         repeat   
        linein          buffer
        cmpi.b          #1,D0   * Checks if the input is one letter
        bne             more
        clr.l           D1
        clr.l           D2
        clr.l           D3
        clr.l           D4
        clr.l           D5
        clr.l           D6
        ori.b           #$20,buffer
        cmpi.b          #'y',buffer
        beq             base1
        cmpi.b          #'n',buffer
        bne             more
        lineout         finish
        
        break                   * Terminate execution
*
*----------------------------------------------------------------------
*       Storage declarations

title:          dc.b    'Program #3, Erick Pulido, cssc0237',0
skipln:         dc.b    0
prompt1:        dc.b    'Enter the base of the number to convert (2..16):',0
prompt2:        dc.b    'Enter the number to convert:',0
prompt3:        dc.b    'Enter the base to convert to:',0
buffer:         ds.b    80
basemsg:        dc.b    'The base is invalid',0
nummsg:         dc.b    'The number is invalid',0
newnum:         ds.b    80
msg:            dc.b    'The number is '
output:         ds.b    80
repeat:         dc.b    'Do you want to convert another number (Y/N)?',0
finish:         dc.b    'The program has finished.',0

        end


